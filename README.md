[![Swift 5](https://img.shields.io/badge/Swift-5-green.svg?style=flat)](https://swift.org/)

Xcode Version 10.2.1
# Project SearchAssignment
the search assignment has a search view with 1 level horizontal tabs filter for countries and a search field above them, User can write search term and select country also he has  the ability to select all countries, The expected result of search is a city record with the location on map.

## Notes
### MVC
This project uses the MVC pattern. 

### Dependencies
Dependencies are provided by CocoaPods.
