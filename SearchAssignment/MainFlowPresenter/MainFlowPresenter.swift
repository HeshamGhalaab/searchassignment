//
//  MainFlowPresenter.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/15/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

struct MainFlowPresenter {
    
    func navigateToSearch(with countries: [Country]){
        let searchVC = searchViewControllerCreation(with: countries)
        let window = getWindow()
        dismissPresntedControllers(in: window)
        setRoot(to: searchVC, for: window)
    }
    
    private func searchViewControllerCreation(with countries: [Country]) -> SearchViewController{
        let searchVC = SearchViewController.make()
        searchVC.filteredCountries = countries
        searchVC.originalCountries = countries
        return searchVC
    }
    
    private func dismissPresntedControllers(in window: UIWindow){
        guard window.rootViewController?.presentedViewController != nil else {return}
        window.rootViewController?.dismiss(animated: false, completion: {
            self.dismissPresntedControllers(in: window)
        })
    }
    
    private func setRoot(to rootViewController: UIViewController, for window: UIWindow){
        window.makeKeyAndVisible()
        window.rootViewController = rootViewController
    }
    
    private func getWindow() -> UIWindow{
        guard var window = (UIApplication.shared.delegate as? AppDelegate)?.window else {
            fatalError("Cannot getting window !")
        }
        window = handlingUI(for: window)
        handlingTransition(for: window)
        return window
    }
    
    private func handlingUI(for window: UIWindow) -> UIWindow{
        window.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        return window
    }
    
    private func handlingTransition(for window: UIWindow){
        UIView.transition(with: window, duration: 0.55001, options: .transitionFlipFromLeft, animations: nil, completion: nil)
    }
}
