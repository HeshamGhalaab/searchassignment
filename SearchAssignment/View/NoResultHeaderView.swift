//
//  NoResultHeaderView.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class NoResultHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var noResultLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
