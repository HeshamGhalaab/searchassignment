//
//  MapForCityViewController.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit
import MapKit

class MapForCityViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    // MARK: Properties
    var city: City?
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    // MARK: Methods
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        guard let city = city else {return}
        cityNameLabel.text = city.name
        
        setupMap(with: city)
    }
    
    private func setupMap(with city: City){
        let coordinate = CLLocationCoordinate2D(latitude: city.latitude, longitude: city.longitude)
        addAnnotation(to: coordinate)
        setRegion(for: coordinate)
    }
    
    private func addAnnotation(to coordinate: CLLocationCoordinate2D){
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
    }
    
    private func setRegion(for coordinate: CLLocationCoordinate2D){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    // MARK: Actions
    @IBAction func onTapClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MapForCityViewController: StoryboardMakeable{
    static var storyboardName: String = Storyboard.Main.rawValue
    typealias StoryboardMakeableType = MapForCityViewController
}
