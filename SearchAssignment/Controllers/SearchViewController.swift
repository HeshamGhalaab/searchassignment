//
//  SearchViewController.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit
import Parchment

class SearchViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: Properties
    private let pagingViewController = PagingViewController<PagingIndexItem>()
    private var currentSelectedIndex = 0
    var filteredCountries = [Country]()
    var originalCountries = [Country]()
    
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        pagingViewController.dataSource = self
        pagingViewController.delegate = self
        searchBar.delegate = self
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        setupSearchBar()
        setupPagingViewController()
    }
    
    private func setupSearchBar(){
        searchBar.placeholder = "Find a city..."
    }
    
    private func setupPagingViewController(){
        addChild(pagingViewController)
        containerView.addSubview(pagingViewController.view)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.topAnchor
                .constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor, constant: 0),
            pagingViewController.view.bottomAnchor
                .constraint(equalTo: containerView.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            pagingViewController.view.trailingAnchor
                .constraint(equalTo: containerView.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            pagingViewController.view.leadingAnchor
                .constraint(equalTo: containerView.safeAreaLayoutGuide.leadingAnchor, constant: 0)
            ])
        pagingViewController.didMove(toParent: self)
    }
    
    private func search(forKey key: String?){
        setKeyForCurrentSelectedCountry(with: key)
        
        guard let key = searchBar.text, !key.isEmpty else{
            loadOriginalCountries()
            return
        }
        
        guard currentSelectedIndex != 0 else {
            searchInAllCountries(forKey: key)
            return
        }
        
        let filteredCountry = filteredCountries[currentSelectedIndex]
        let cities = filteredCountry.cities.filter({ $0.name.lowercased().contains(key.lowercased()) })
        filteredCountries[currentSelectedIndex].cities = cities
        
        self.pagingViewController.reloadData()
    }
    
    /// a specific mechanism for searching in all countries when currentSelectedIndex == 0
    private func searchInAllCountries(forKey key: String){
        // cause searching in a huge data like allCountries will affect the time , so i make the array splitted into chunks inOrder to filter the first splitied array , and then continue in the background thread while at least 20 city appears for the user.
        
        let chunkedCities = splitCities()
        guard chunkedCities.count != 0 else { return }
        var chunkedCitiesIndices = getIndices(of: chunkedCities)
        
        let value = filter(chunkedCities: chunkedCities, forChunkedCitiesIndices: chunkedCitiesIndices, byKey: key)
        let cities = value.0
        chunkedCitiesIndices = value.1
        
        filteredCountries[currentSelectedIndex].cities = cities
        DispatchQueue.main.async {
            self.pagingViewController.reloadData()
        }
        
        filterRemaining(chunkedCities: chunkedCities, forChunkedCitiesIndices: chunkedCitiesIndices, byKey: key)
    }
    
    private func filter(chunkedCities: [[City]], forChunkedCitiesIndices citiesIndices: [Int], byKey key:String) -> ([City], [Int]){
        var indices = citiesIndices
        var cities = [City]()
        // using while loop to make at least 20 city appears for the user.
        while cities.count >= 0, cities.count <= 20 {
            guard let index = indices.first else {
                print("Loop breaked")
                break
            }
            
            let filteredCities = chunkedCities[index].filter({ $0.name.lowercased().contains(key.lowercased()) })
            cities.append(contentsOf: filteredCities)
            print("at index: \(index), filteredCities count is: \(filteredCities.count), cities count is: \(cities.count)")
            if cities.count >= 0, cities.count <= 20{
                indices.removeFirst()
            }
        }
        // return filteredCities and the rest of chunkedCitiesIndices
        return (cities, indices)
    }
    
    private func filterRemaining(chunkedCities: [[City]], forChunkedCitiesIndices citiesIndices: [Int], byKey key: String){
        guard citiesIndices.count > 1 else {return}
        let dispatchQueue = DispatchQueue.global(qos: .userInitiated)
        dispatchQueue.async {
            for value in citiesIndices{
                print("chunkedCitiesCount now will have \(value)")
                let cities = chunkedCities[value].filter({ $0.name.lowercased().contains(key.lowercased()) })
                self.filteredCountries[self.currentSelectedIndex].cities.append(contentsOf: cities)
            }
            
            DispatchQueue.main.async {
                self.pagingViewController.reloadData()
            }
        }
    }
    
    private func splitCities() -> [[City]]{
        let filteredCountry = filteredCountries[currentSelectedIndex]
        let chunkedNumber = filteredCountry.cities.count / 15
        let chunkedCities = filteredCountry.cities.chunked(into: chunkedNumber)
        print("chunkedCities count: \(chunkedCities.count), chunkedNumber: \(chunkedNumber)")
        return chunkedCities
    }
    
    private func getIndices(of chunkedCities: [[City]]) -> [Int]{
        var chunkedCitiesIndices = [Int]()
        for i in chunkedCities.indices{
            chunkedCitiesIndices.append(i)
        }
        return chunkedCitiesIndices
    }
    
    private func setKeyForCurrentSelectedCountry(with key: String?){
        filteredCountries[currentSelectedIndex].searchKey = key ?? ""
        originalCountries[currentSelectedIndex].searchKey = key ?? ""
    }
    
    private func loadOriginalCountries(){
        filteredCountries[currentSelectedIndex].cities = originalCountries[currentSelectedIndex].cities
        pagingViewController.reloadData()
    }
}

extension SearchViewController: StoryboardMakeable{
    static var storyboardName: String = Storyboard.Main.rawValue
    typealias StoryboardMakeableType = SearchViewController
}

extension SearchViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        self.search(forKey: searchBar.text)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        if let key = searchBar.text, key.isEmpty {
            setKeyForCurrentSelectedCountry(with: "")
            loadOriginalCountries()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if isBackSpacePressed(in: text){
            print("isBack space !")
            self.filteredCountries[currentSelectedIndex].cities = originalCountries[currentSelectedIndex].cities
        }
        return true
    }
    
    private func isBackSpacePressed(in text: String) -> Bool{
        guard let char = text.cString(using: String.Encoding.utf8) else { return false}
        let isBackSpace = strcmp(char, "\\b")
        guard (isBackSpace == -92) else { return false }
        return true
    }
}

extension SearchViewController: PagingViewControllerDataSource {
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T {
        return PagingIndexItem(index: index, title: filteredCountries[index].countryName) as! T
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        let citiesController = CitiesTableViewController.make()
        citiesController.country = filteredCountries[index]
        return citiesController
    }
    
    func numberOfViewControllers<T>(in: PagingViewController<T>) -> Int {
        return filteredCountries.count
    }
}

extension SearchViewController: PagingViewControllerDelegate {
    
    // handle the size of our paging items to be equal to the width of the country title.
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, widthForPagingItem pagingItem: T, isSelected: Bool) -> CGFloat? {
        guard let item = pagingItem as? PagingIndexItem else { return 0 }
        
        let insets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: pagingViewController.menuItemSize.height)
        let attributes = [NSAttributedString.Key.font: pagingViewController.font]
        
        let rect = item.title.boundingRect(with: size,
                                           options: .usesLineFragmentOrigin,
                                           attributes: attributes,
                                           context: nil)
        
        let width = ceil(rect.width) + insets.left + insets.right
        
        if isSelected {
            return width * 1.5
        } else {
            return width
        }
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, didScrollToItem pagingItem: T, startingViewController: UIViewController?, destinationViewController: UIViewController, transitionSuccessful: Bool){
        guard let item = pagingItem as? PagingIndexItem else { return }
        self.currentSelectedIndex = item.index
        self.searchBar.text = filteredCountries[item.index].searchKey
    }
}
