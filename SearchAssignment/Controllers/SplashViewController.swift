//
//  SplashViewController.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareCountries()
    }
    
    func prepareCountries(){
        let dispatchQueue = DispatchQueue.global(qos: .userInitiated)
        dispatchQueue.async {
            let provider = CountryDataProvider()
            guard let objects = provider.parseCountriesJson() else {return}
            let countries = provider.prepareCountriesModel(from: objects)
            self.navigateToSearch(with: countries)
        }
    }
    
    private func navigateToSearch(with countries: [Country]){
        DispatchQueue.main.async {
            MainFlowPresenter().navigateToSearch(with: countries)
        }
    }
}
