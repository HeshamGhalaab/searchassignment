//
//  CitiesTableViewController.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class CitiesTableViewController: UITableViewController {

    // MARK: Properties
    var country: Country?
    let noResultHeaderViewHeight: CGFloat = 150
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        tableView.tableFooterView = UIView()
        
        if let country = country, country.cities.count == 0{
            let nib = UINib(nibName: "NoResultHeaderView", bundle: nil)
            tableView.register(nib, forHeaderFooterViewReuseIdentifier: "NoResultHeaderView")
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return country?.cities.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        guard let country = country else { return cell }
        let cityName = country.cities[indexPath.row].name
        
        let normalAttribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .regular),
                               NSAttributedString.Key.foregroundColor : UIColor.darkGray]
        
        if !country.searchKey.isEmpty{
            let ranges = cityName.ranges(of: country.searchKey, options: .caseInsensitive)
            let searchedAttribute = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17),
                                     NSAttributedString.Key.foregroundColor : UIColor.black]
            let myMutableString = NSMutableAttributedString(string: cityName, attributes: normalAttribute)
            for range in ranges{
                let nsRange = NSRange(range, in: "")
                myMutableString.addAttributes(searchedAttribute, range: nsRange)
            }
            cell.textLabel?.attributedText = myMutableString
        }else{
            let myMutableString = NSMutableAttributedString(string: cityName, attributes: normalAttribute)
            cell.textLabel?.attributedText = myMutableString
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let country = country, country.cities.count == 0 else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NoResultHeaderView")
        if let customView = view as? NoResultHeaderView{
            customView.noResultLabel.text = "Sorry no results found \nPlease try another keyword."
            return customView
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let country = country, country.cities.count == 0 else { return 0 }
        return noResultHeaderViewHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedCity = country?.cities[indexPath.row] else { return }
        let presenter = SearchPresenter(vc: self)
        presenter.present(.map(forCity: selectedCity))
    }
    
}

extension CitiesTableViewController: StoryboardMakeable{
    static var storyboardName: String = Storyboard.Main.rawValue
    typealias StoryboardMakeableType = CitiesTableViewController
}
