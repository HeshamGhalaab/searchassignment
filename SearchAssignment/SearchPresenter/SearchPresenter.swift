//
//  SearchPresenter.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/15/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class SearchPresenter{
    
    // Here we define a set of supported destinations using an enum
    enum Destination {
        case map(forCity: City)
    }
    
    // In most cases it's totally safe to make this a strong
    // reference, but in some situations it could end up
    // causing a retain cycle, so better be safe than sorry :)
    private weak var viewController: UIViewController?
    
    // MARK: - Initializer
    init(vc: UIViewController) {
        self.viewController = vc
    }
    
    // MARK: - Navigator
    func present(_ destination: Destination) {
        presentViewController(for: destination)
    }
    
    // MARK: - Private
    private func presentViewController(for destination: Destination) {
        switch destination {
        case .map(let city):
            presentMapForCityViewController(with: city)
        }
    }
    
    private func presentMapForCityViewController(with city: City){
        let mapForCityVC = MapForCityViewController.make()
        mapForCityVC.city = city
        mapForCityVC.modalPresentationStyle = .overFullScreen
        mapForCityVC.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.async {
            self.viewController?.present(mapForCityVC, animated: true, completion: nil)
        }
    }
}
