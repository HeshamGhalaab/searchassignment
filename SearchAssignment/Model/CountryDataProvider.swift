//
//  CountryDataProvider.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/15/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import Foundation

enum CountryKeyConstant: String{
    case countryName = "country"
    case cityName = "name"
    case latitude = "lat"
    case longitude = "lng"
}

struct CountryDataProvider {
    
    /// parse countries json after loading the file to be ready as [[String: String]] for creating countries objects.
    func parseCountriesJson() -> [[String: String]]? {
        do {
            guard let jsonFile = loadCountriesJsonFile() else {return nil}
            let data = try Data(contentsOf: jsonFile)
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard let objects = jsonObject as? [[String: String]] else { return nil}
            return objects
        } catch {
            assert(true, error.localizedDescription)
            return nil
        }
    }
    
    private func loadCountriesJsonFile() -> URL?{
        let jsonFile = Bundle.main.url(forResource: "DataSet", withExtension: "json")
        return jsonFile
    }
    
    func prepareCountriesModel(from objects: [[String: String]]) -> [Country]{
        var countries = [Country]()
        print("Starting countries model creation.")
        
        // starting our array with "All" which will have all the cities.
        countries.append(Country(countryName: "All", searchKey: "", cities: []))
        
        for object in objects{
            
            let countryName = object[CountryKeyConstant.countryName.rawValue] ?? ""
            let cityName = object[CountryKeyConstant.cityName.rawValue] ?? ""
            let lat = Double(object[CountryKeyConstant.latitude.rawValue] ?? "0") ?? 0
            let long = Double(object[CountryKeyConstant.longitude.rawValue] ?? "0") ?? 0
            
            let city = City(name: cityName, latitude: lat, longitude: long)
            
            // I use lastCountryIndex here because the cities from the same country ordered below each other.
            let lastCountryIndex = countries.count - 1
            if countries.count > 0, countries[lastCountryIndex].countryName == countryName{
                countries[lastCountryIndex].cities.append(city)
            }else{
                let country = Country(countryName: countryName, searchKey: "", cities: [city])
                countries.append(country)
            }
            
            // append the city to "All"
            countries[0].cities.append(city)
        }
        
        print("Countries model has been created.")
        return countries
    }
    
    
    
}
