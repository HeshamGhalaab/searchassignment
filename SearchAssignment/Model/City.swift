//
//  City.swift
//  SearchAssignment
//
//  Created by hesham ghalaab on 7/14/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import Foundation

struct City {
    let name: String
    let latitude: Double
    let longitude: Double
}
